import EventManager from './EventManager.class';
import EventResponse from './lib/EventResponse';

export { EventManager, EventResponse };

// const evtConfig = {
//   azure: {
//     name: 'samsoft-topic',
//     subscription: ['samsoft-email-sub', 'samsoft-blockchain-sub'],
//     connectionString:
//       'Endpoint=sb://testsamsoft.servicebus.windows.net/;SharedAccessKeyName=manage-topic;SharedAccessKey=yLsjiQhnwhMi3aTKpDe3WUlrGF1ZtfCTzRAiwDlS11c=;EntityPath=samsoft-topic'
//   }
// };

// const eventMgr = EventManager.getInstance();
// eventMgr.initialize(evtConfig);

// eventMgr.on('samsoft-blockchain-sub', (result: EventResponse) => {
//   console.log(result.getBody());
//   result.complete();
// });

// eventMgr.emit(['samsoft-email-sub:notification', 'samsoft-blockchain-sub'], {
//   body: { name: 'samuel', type: 'external', method: 'name' },
//   source: 'node'
// });
