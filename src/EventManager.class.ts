/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable array-callback-return */
import { camelCase } from 'lodash';

import emittery from './lib/event';
import { IEmitterInterface, IEventConfig } from './lib/interfaces';
import AzureServiceBus from './services/AzureServiceBus';
import ServiceMq from './services/Index';
import EventResponse from './lib/EventResponse';

export default class EventManager {
  private static instance: EventManager;

  private _emittery!: IEmitterInterface;

  private _serviceBus!: AzureServiceBus;

  private constructor() {}

  static getInstance(): EventManager {
    if (!EventManager.instance) {
      EventManager.instance = new EventManager();

      EventManager.instance._emittery = emittery;
    }

    return EventManager.instance;
  }

  public initialize(config: IEventConfig = {} as any) {
    if (config && config.azure) {
      this._serviceBus = ServiceMq('azure', config, this._emittery);
      this._serviceBus.init();
    }
  }

  public on(eventName, listener) {
    this._emittery.addListener(camelCase(eventName), listener);
  }

  // eslint-disable-next-line consistent-return
  public emit(eventNames: string | string[], payload: any) {
    // if (!this._serviceBus) throw new Error('Event manager config error');

    // eventNames => sub:parent:child
    if (this._serviceBus) return this._serviceBus.sender(eventNames, payload);

    const sendEventToListener = (eventName: string) => {
      //
      Object.assign(payload, {
        body: { data: { ...payload.body }, source: 'node' },
        label: eventName
      });

      this._emittery.emit({
        type: camelCase(eventName),
        result: new EventResponse({ response: payload, error: undefined })
      });
    };

    const listOfEvents = Array.isArray(eventNames) ? eventNames : [eventNames];
    listOfEvents.map((eventName: string) => sendEventToListener(eventName));
  }

  async close() {
    if (this._serviceBus) {
      await this._serviceBus.close();
    }
  }
}
