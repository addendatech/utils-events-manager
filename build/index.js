"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventResponse = exports.EventManager = void 0;
var EventManager_class_1 = __importDefault(require("./EventManager.class"));
exports.EventManager = EventManager_class_1.default;
var EventResponse_1 = __importDefault(require("./lib/EventResponse"));
exports.EventResponse = EventResponse_1.default;
// const evtConfig = {
//   azure: {
//     name: 'samsoft-topic',
//     subscription: ['samsoft-email-sub', 'samsoft-blockchain-sub'],
//     connectionString:
//       'Endpoint=sb://testsamsoft.servicebus.windows.net/;SharedAccessKeyName=manage-topic;SharedAccessKey=yLsjiQhnwhMi3aTKpDe3WUlrGF1ZtfCTzRAiwDlS11c=;EntityPath=samsoft-topic'
//   }
// };
// const eventMgr = EventManager.getInstance();
// eventMgr.initialize(evtConfig);
// eventMgr.on('samsoft-blockchain-sub', (result: EventResponse) => {
//   console.log(result.getBody());
//   result.complete();
// });
// eventMgr.emit(['samsoft-email-sub:notification', 'samsoft-blockchain-sub'], {
//   body: { name: 'samuel', type: 'external', method: 'name' },
//   source: 'node'
// });
