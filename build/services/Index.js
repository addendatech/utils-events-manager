"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var interfaces_1 = require("../lib/interfaces");
var AzureServiceBus_1 = __importDefault(require("./AzureServiceBus"));
exports.default = (function (service) {
    var payload = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        payload[_i - 1] = arguments[_i];
    }
    if (service === interfaces_1.ServiceType.AZURE) {
        return new AzureServiceBus_1.default(payload[0], payload[1]);
    }
    throw new Error('Unsupported service');
});
