import AzureServiceBus from './AzureServiceBus';
declare const _default: (service: any, ...payload: any[]) => AzureServiceBus;
export default _default;
