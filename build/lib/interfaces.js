"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceType = void 0;
var ServiceType;
(function (ServiceType) {
    ServiceType["AZURE"] = "azure";
    ServiceType["RABBITMQ"] = "rabbitmq";
})(ServiceType = exports.ServiceType || (exports.ServiceType = {}));
